package com.orderservice.OrderService.entities;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String orderId;
    private Long userId;

    public OrderEntity(Long userId, String orderId) {
        this.userId = userId;
        this.orderId = orderId;
    }

    public OrderEntity(Long id, Long userId, String orderId) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
    }

    public OrderEntity() {

    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
