package com.orderservice.OrderService.services;

import com.orderservice.OrderService.dtos.OrderDTO;
import com.orderservice.OrderService.entities.OrderEntity;
import com.orderservice.OrderService.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 *=============================================================
 * This service class is used to handle all the business logic
 * related to orders within the application
 *=============================================================
 *
 * @author  Priyantha Buddhika
 * @version 1.0
 * @since   2021-11-08
 */
@Service
public class OrderService {
    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    private final OrderRepository repository;

    @Autowired
    public OrderService(OrderRepository repository) {
        this.repository = repository;
    }

    /**
     * ==============================================
     * This method is used to create a new order
     * =============================================
     *
     * @param order orderDTO
     * @return ResponseEntity with a success or error message
     * @since 1.0
     */
    public ResponseEntity<String> createOrder(OrderDTO order) {
        LOGGER.info("==========Entered into createOrder method in OrderService============");
        try {
            OrderEntity orderEntity = new OrderEntity(order.getUserId(), order.getOrderId());
            repository.save(orderEntity);

            LOGGER.info("Order created successfully : {}", orderEntity.getOrderId());

            return new ResponseEntity<>(
                    "Order created successfully!",
                    HttpStatus.CREATED
            );
        } catch (Exception e) {
            LOGGER.error("ERROR creating order : " + e.getMessage());
            return new ResponseEntity<>(
                    "Something went wrong!",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * =================================================
     * This method is used to update an existing order
     * ================================================
     *
     * @param order orderDTO
     * @return ResponseEntity with a success or error message
     * @since 1.0
     */
    public ResponseEntity<String> updateOrder(OrderDTO order) {
        LOGGER.info("==========Entered into updateOrder method in OrderService============");
        try {
            if (order.getId() == null)
                return new ResponseEntity<>("ID can't be empty", HttpStatus.BAD_REQUEST);

            OrderEntity orderEntity = repository.getById(order.getId());
            orderEntity.setOrderId(order.getOrderId());
            orderEntity.setUserId(order.getUserId());
            repository.save(orderEntity);

            LOGGER.info("Order updated successfully : {}", orderEntity.getOrderId());

            return new ResponseEntity<>(
                    "Order updated successfully!",
                    HttpStatus.OK
            );
        } catch (Exception e) {
            LOGGER.error("ERROR creating order : " + e.getMessage());
            return new ResponseEntity<>(
                    "Something went wrong!",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * ==========================================================
     * This method is used to get all orders for a given userId
     * ==========================================================
     *
     * @param userId orderDTO
     * @return ResponseEntity with list of Orders or error
     * @since 1.0
     */
    public ResponseEntity<List<OrderDTO>> getOrdersByUserId(Long userId) {
        LOGGER.info("==========Entered into getOrdersByUserId method in OrderService============");

        List<OrderDTO> orders = null;

        try {
            orders = repository.findOrdersByUserId(userId)
                    .stream()
                    .map(order -> new OrderDTO(
                            order.getId(),
                            order.getUserId(),
                            order.getOrderId()
                    )).collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error("ERROR fetching orders : {}", e.getMessage());
            return new ResponseEntity<>(
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }


}
